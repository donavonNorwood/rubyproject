class HomeController < ApplicationController
    def index
    end
    
    def about
    end
    
    def edit
    end
    
    def sign_up
    end
    
    def users
    end
    
    
    def cancel
    end

    def password
    end
    
    def sign_in
    end
    
    def sign_out
    end    
end