Rails.application.routes.draw do
  resources :pins
  devise_for :users
  get  'home/index'
  root 'pins#index'
  get  'home/about'
  get  'devise/sessions/new'
  get  'devise/passwords/edit'
  get  'devise/passwords/new'
  get  'devise/registrations/edit'
  get  'devise/registrations/new'
  get  'devise/confirmations/new'
  get  'devise/mailer/confirmation'
  get  'devise/mailer/password_change'
  get  'devise/mailer/reset_password_instructions'
  get  'devise/mailer/unlock_instructions'
end
